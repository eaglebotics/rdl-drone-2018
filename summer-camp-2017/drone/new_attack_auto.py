from EagleDrone import Eagle_Drone

eagle_drone = Eagle_Drone(speed_scale=22)

try:
    move = {"x_axis": 0, "y_axis": 0, "z_axis": 0, "turning": 0}
    eagle_drone.drone.takeoff()

    # # Correct leftward lust
    #
    # move["x_axis"] = 1
    # eagle_drone.move_drone(1, move)
    # move["x_axis"] = 0
    # eagle_drone.move_drone(0, move)
    # move["turning"] = 1
    # eagle_drone.move_drone(0.5, move)
    # move["turning"] = 0
    # eagle_drone.move_drone(0, move)

    # Move down to seesaw optimum

    eagle_drone.reset_movement()
    move["y_axis"] = -1
    eagle_drone.move_drone(1.7, move)
    move["y_axis"] = 0
    eagle_drone.move_drone(0, move)
    move["x_axis"] = 1
    eagle_drone.move_drone(1, move)
    move["x_axis"] = 0
    eagle_drone.reset_movement()

    # Go over saw

    move["z_axis"] = 1
    eagle_drone.move_drone(5, move)
    move["z_axis"] = -1
    eagle_drone.move_drone(2, move)
    move["z_axis"] = 0
    eagle_drone.move_drone(1, move)

    # # Correct rightward lust
    # move["turning"] = -1
    # eagle_drone.move_drone(1, move)
    # move["turning"] = 0
    # eagle_drone.move_drone(0, move)

    # Back over

    move["z_axis"] = -1
    eagle_drone.move_drone(3, move) # longer than over-saw phase because of the need to overcome forward momentum
    move["z_axis"] = 0
    eagle_drone.move_drone(1, move)

    eagle_drone.drone.land()
    # movement = {"x_axis": 0, "y_axis": 0, "z_axis": 1, "turning": 0}
    # eagle_drone.drone.takeoff()
    # print("Preparing to move along Z axis")
    # eagle_drone.move_drone(1, movement)
    # print("Moved along Z Axis")
    # movement = {"x_axis": 0, "y_axis": 0, "z_axis": 0, "turning": 0}
    # eagle_drone.move_drone(0, movement)
    # eagle_drone.drone.land()

    # Close the window and quit.
    eagle_drone.drone_stop()
except:
    eagle_drone.drone.land()