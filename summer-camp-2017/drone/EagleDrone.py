"""
Wrapper class for the Parrot Bebop API. Intended to be easier to use. Thank god.

Here's your documentation:
  * Want to just start the damn thing up, regardless of auto/teleop? --- drone_init()
  * Enable video specifically? ----------------------------------------- video_init()
  * Control panel/screen need manual enablin'? ------------------------- pygame_screen_init()
  * TeleOp? ------------------------------------------------------------ teleop()
  * Autonomous? That's in its own file in this here folder.
  * Move the drone within autonomous modes? ---------------------------- move_drone()
  * All done? ---------------------------------------------------------- drone_stop()
"""

import pygame
import cv2
from commands import *
import logging
from bebop import Bebop

# NOTE: items prefixed with "__" are NOT intended for your use.


class Eagle_Drone:
    def __init__(self, speed_scale=60):
        """Constructor. Called by the user with something like 'var = Eagle_Drone()'.

        Keyword arguments:
        speed_scale -- Controls the speed at which drone flies. 70 is fast, 50 is normal.
        """

        # Prints important info on drone operations
        logging.basicConfig(level=logging.DEBUG)

        # Connect to drone via WiFi
        self.drone = Bebop()

        self.scale = speed_scale
        self.drone_init()

    def drone_init(self):
        """Initializes the various components needed to control the drone, regardless of OpMode."""

        # self.video_init()
        self.pygame_screen_init()
        self.check_for_joysticks()
        print("All drone startup checks have passed. Awaiting instructions.")

    def drone_stop(self):
        """Stops the drone by landing it and closing the screen/dashboard."""

        self.drone.land()
        # self.drone.videoDisable()
        pygame.quit()

    def emergency_check(self):
        """Checks if the back button has been pressed on the joystick, meaning the drone should land ("killswitch").

        This method is mostly intended for autonomous, where the joystick is mostly disregarded. Insert this check
        when possible to help prevent accidents.
        """

        if not self.joystick:
            self.drone.emergency()
            self.drone.land()
            raise Exception("Joystick not found; killswitch inoperable")

        if self.joystick.get_button(6) == 1:
            print("Landing...")
            if self.drone.flyingState is None or self.drone.flyingState == 1:
                self.drone.emergency()
            self.drone.land()

    def video_init(self):
        """Initializes the video feed."""

        wnd = None
        fns = self.__video_functions()
        self.drone.video_callbacks(fns[0], fns[1], fns[2])
        self.drone.videoEnable()

    def __video_functions(self):
        """Contains the functions needed to initialize the video feed."""

        def video_frame(frame):
            cv2.imshow("Drone", frame)
            cv2.waitKey(10)

        def video_start():
            print("Starting video...")

        def video_end():
            print("Ending video...")
            cv2.destroyWindow("Drone")
            # Have to send waitKey several times on Unix to make window disappear
            for i in range(1, 5):
                cv2.waitKey(1)

        return video_start, video_end, video_frame

    def pygame_screen_init(self, size=(100, 100)):
        """Starts the pygame screen."""

        pygame.init()
        self.screen = pygame.display.set_mode(size)
        pygame.display.set_caption("Drone Teleop")
        self.clock = pygame.time.Clock()

    def check_for_joysticks(self):
        if pygame.joystick.get_count() == 0:
            raise Exception("No joysticks found")
        else:
            self.joystick = pygame.joystick.Joystick(0)
            self.joystick.init()
            print("Initialized %s" % (self.joystick.get_name()))

    def __update(self, x_axis, y_axis, z_axis, turning):
        """The drone's movement is controlled by changing variables which are then sent to the drone.

        These variables represent 3-dimensional movement as well as turning.

        1 indicates maximum forward power, 0 indicates no power, and -1 indicates maximum reverse power.
        """

        self.drone.update(cmd=movePCMDCmd(True, x_axis*self.scale,
                                          z_axis*self.scale, turning*self.scale, y_axis*self.scale))

    def __update_with_dict(self, movement_dict):
        """Uses a more user-friendly dictionary format to control the drone's movement."""

        usable_list = self.__translate_dict(movement_dict)
        self.__update(usable_list[0], usable_list[1],
                      usable_list[2], usable_list[3])

    def __translate_dict(self, dict_in):
        """Takes a dictionary containing movement variables and converts it to a computer-readable list."""

        li_out = [None, None, None, None]
        for key in dict_in.keys():
            destination_index = 0
            if key == "x_axis":
                destination_index = 0
            elif key == "z_axis":
                destination_index = 2
            elif key == "y_axis":
                destination_index = 1
            elif key == "turning":
                destination_index = 3
            else:
                raise Exception("Improperly keyed dictionary.")

            li_out[destination_index] = dict_in[key]

        return li_out

    def move_drone(self, wait_time, movement_dict):
        """Updates the drone's movement based on the provided dictionary in autonomous mode.

        This method exists mainly for safety reasons, as it includes emergency checks.

        Arguments:
        wait_time -- In seconds, how long the drone should continue moving uninterrupted based on movement_dict.
        movement_dict -- {"x_axis": n, "y_axis": n, "z_axis": n, "turning": n}. 1 >= n >= -1. 0 = no movement.
                         Refer to __update()'s documentation for more info on what n represents.
                         NOTE: THE DRONE DOES NOT STOP ON ITS OWN IN AUTONOMOUS. RESET VALUES (n) TO 0 WHEN NOT NEEDED.
                         Don't learn this the hard way and spend 3 hours and/or $549.99 in repairs.
        """

        # Before proceeding, make sure killswitch not triggered
        self.emergency_check()

        self.__update_with_dict(movement_dict)
        if wait_time > 0:
            self.drone.wait(wait_time, self.emergency_check)
        self.emergency_check()

    def reset_movement(self):
        self.drone.update(cmd=movePCMDCmd(False, 0, 0, 0, 0), functocheck=self.emergency_check)
        self.drone.update(cmd=None)

    def teleop(self):
        """Grants full control of the drone to the joystick until the pygame window is closed or Python is kill -9'd.

        This code was written in the 2017-18 school year, and is considered legacy because it is god-awful.
        90% of it isn't mine. I will attempt to document its many idiosyncrasies, but I will fail because it's garbage.
        """

        if not self.joystick:
            self.check_for_joysticks()

        done = False
        while not done:
            try:
                # EVENT PROCESSING STEP
                for event in pygame.event.get():  # User did something
                    if event.type == pygame.QUIT:  # If user clicked close
                        done = True  # Flag that we are done so we exit this loop

                executing_command = False

                # BACK Button
                if self.joystick.get_button(6) == 1:
                    executing_command = True
                    print("Landing...")
                    if self.drone.flyingState is None or self.drone.flyingState == 1:  # if taking off then do emegency landing
                        self.drone.emergency()
                    self.drone.land()

                # Right Bumper
                if self.joystick.get_button(5) == 1:
                    executing_command = True
                    self.drone.update(cmd=movePCMDCmd(
                        True, 0, 0, self.scale, 0))
                    print("Drone spinning clockwise.")

                # Left Bumper
                if self.joystick.get_button(4) == 1:
                    executing_command = True
                    self.drone.update(cmd=movePCMDCmd(
                        True, 0, 0, -1 * self.scale, 0))
                    print("Drone spinning counterclockwise.")

                # START Button
                if self.joystick.get_button(7) == 1:
                    executing_command = True
                    print("Taking off..")
                    self.drone.takeoff()

                # Left joystick (y-axis)
                if abs(self.joystick.get_axis(1)) > 0.05:
                    executing_command = True
                    print("X & Z axes")
                    self.drone.update(cmd=movePCMDCmd(
                        True, 0, 0, 0, self.joystick.get_axis(1) * self.scale * -1))

                # Right Joystick
                if abs(self.joystick.get_axis(3)) > 0.05 or abs(self.joystick.get_axis(4)) > 0.05:
                    executing_command = True
                    self.drone.update(cmd=movePCMDCmd(True, 1 * self.joystick.get_axis(3) * self.scale,
                                                 -1 * self.joystick.get_axis(4) * self.scale, 0, 0))

                (hat_x, hat_y) = self.joystick.get_hat(0)
                if (hat_x != 0 or hat_y != 0):
                    executing_command = True
                    print("Hat %d, %d" % (hat_x, hat_y))

                # Limit to 20 frames per second
                self.clock.tick(20)

                if not executing_command:
                    self.drone.update(cmd=movePCMDCmd(False, 0, 0, 0, 0))
                    self.drone.update(cmd=None)
            except Exception as e:
                print("Error: " + type(e).__name__)
                if self.drone.flyingState is None or self.drone.flyingState == 1:  # if taking off then do emegency landing
                    self.drone.emergency()
                self.drone.land()
                done = True

        # Close the window and quit.
        self.drone_stop()
