#! /usr/bin/env bash

#### WARNING ####
# This script kills *all* running instances of python2.7.

ID=`ps -ef | grep python2.7 | awk '{print $2}'`
kill -9 $ID
